import sys
import argparse
import random
import string

def params_parser():
  parser = argparse.ArgumentParser()
  parser.add_argument('--output', type=argparse.FileType('w', encoding='UTF-8'))
  parser.add_argument('--lines', type=int, default=100)
  parser.add_argument('--min_length', type=int, default=10)
  parser.add_argument('--max_length', type=int, default=20)

  return parser

def generate_file(params):
  output = params.output
  lines = params.lines
  min_length = params.min_length
  max_length = params.max_length

  symbol_choises = string.ascii_letters + string.digits
  print(f'\rLines: 0/{lines}', end='')

  for i in range(1, lines+1):
    rand_length = random.randint(min_length, max_length)
    rand_str = ''.join(random.choices(symbol_choises, k=rand_length))

    output.write(f'{rand_str}\n')

    if not (i % 1000):
      print(f'\rLines: {i}/{lines}', end='')

def main():
  parser = params_parser()
  params = parser.parse_args(sys.argv[1:])
  if not params.output or not params.lines:
    parser.print_help()
    return

  generate_file(params)

if __name__ == '__main__':
  main()