# file_sorter

Sorting of large files.

Python version: 3.7.0

Generator usage:
```
> python gen_file.py --output="data/rand_1000000.txt" --lines=1000000 [--min_length=10] [--max_length=20]

--output - output file
--lines - number of lines to generate
--min_length - min size of generated lines
--max_length - max size of generated lines
```
Sorter usage:
```
> python sorter.py --input="data/rand_1000000.txt" --output="data/sorted_1000000.txt" [--buffer_size=1024000]

--output - input file
--output - output file
--buffer_size - size of buffer, which will be stored in RAM (measured in symbols)
```