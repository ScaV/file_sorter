from tempfile import TemporaryFile
import os


class MergeSorter:
  def __init__(self, input_file, output_file, buffer_size=None):
    if buffer_size is None:
      buffer_size = 1024 * 1024

    self._input = input_file
    self._output = output_file
    self._buff_max_size = buffer_size
    self._buff_lines = []
    self._temp_files = []
    self._processed = 0

    self._input.seek(0, os.SEEK_END)
    self._file_size = self._input.tell()
    self._input.seek(0, os.SEEK_SET)

  def sort(self, out=None):
    print(f'Buffer size: {self._buff_max_size}')

    for line in self._input:
      self.__process_line(line)

    self.__dump_buffer()
    self.__merge()
    self.__check_sorted(self._output)

  def __check_sorted(self, sorted_file):
    print('Checking if result sorted...')
    sorted_file.seek(0, os.SEEK_SET)
    prev_line = None

    for cur_line in sorted_file:
      if prev_line and cur_line < prev_line:
        raise Exception('Result is not sorted!')
      prev_line = cur_line

    print('OK')
    return True

  def __merge(self):
    print('Merging...')
    while(self._temp_files):
      print(f'\rCurrent iteration files: {len(self._temp_files)}')
      self.__merge_iteration()

  def __merge_iteration(self):
    '''
    Iterate list of temp files both from end and from beginning.
    Files from end of list will be closed and deleted
    Files from beginning will be closed, and replaced by merge result
    So result of merge iteration - number of files in "_temp_files" will be / 2
    '''
    files_count = len(self._temp_files)
    if files_count <= 2:
      # if source file is too small, and there are nothing to merge
      # we are just merge empty file 
      if files_count == 1:
        self._temp_files.append(TemporaryFile(mode='r+'))
      print(f'temp[0] += temp[1]')
      self.__merge_files(self._temp_files[0],
                         self._temp_files[1],
                         self._output)
      self._temp_files = []
      return

    idx_to = 0
    idx_from = len(self._temp_files) - 1
    while idx_to < idx_from:
      print(f'temp[{idx_to}] += temp[{idx_from}]')
      self._temp_files[idx_to] = self.__merge_files(self._temp_files[idx_to],
                                                    self._temp_files[idx_from])

      del self._temp_files[idx_from]
      idx_to += 1
      idx_from -= 1

  def __merge_files(self, file1, file2, file_out=None):
    if file_out is None:
      file_out = TemporaryFile(mode='w+')

    file1.seek(0, os.SEEK_SET)
    file2.seek(0, os.SEEK_SET)

    ln1 = file1.readline()
    ln2 = file2.readline()
    while ln1 and ln2:
      if ln1 < ln2:
        file_out.write(ln1)
        ln1 = file1.readline()
      else:
        file_out.write(ln2)
        ln2 = file2.readline()

    if ln1 or ln2:
      tail, tl = (file1, ln1) if ln1 else (file2, ln2)
      while tl:
        file_out.write(tl)
        tl = tail.readline()

    file1.close()
    file2.close()
    return file_out

  def __process_line(self, line):
    '''
    Collect lines to a buffer
    Dumps buffer if it is full
    '''

    if not self._buff_lines:
      self._buff_size = 0

    if self._buff_size >= self._buff_max_size:
      self.__dump_buffer()

    self._buff_size += len(line)
    self._buff_lines.append(line)

  def __dump_buffer(self):
    '''
    Write sorted buffer to a temporary file
    '''

    if not self._buff_lines:
      return

    self._buff_lines.sort()
    tmp_out = TemporaryFile(mode='w+')
    for line in self._buff_lines:
      tmp_out.write(line)

    tmp_out.seek(0)
    self._temp_files.append(tmp_out)

    self._buff_lines = []
    self._processed += self._buff_size
    self._buff_size = 0

    print(f'\rProcessed: {self._processed}/{self._file_size} ({len(self._temp_files)} file(s))') 
