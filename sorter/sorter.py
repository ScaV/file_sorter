import sys
import argparse
from merge_sorter import MergeSorter

def params_parser():
  parser = argparse.ArgumentParser()
  parser.add_argument('--input', type=argparse.FileType('r', encoding='UTF-8'))
  parser.add_argument('--output', type=argparse.FileType('w+', encoding='UTF-8'))
  parser.add_argument('--buffer_size', type=int, default=1024*1024)

  return parser

def sort_file(params):
  sorter = MergeSorter(params.input, params.output, buffer_size=params.buffer_size)
  sorter.sort()

def main():
  parser = params_parser()
  params = parser.parse_args(sys.argv[1:])

  sort_file(params)

if __name__ == '__main__':
  main()